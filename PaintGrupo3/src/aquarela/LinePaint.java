package aquarela;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;

public class LinePaint extends PencilPaint{

	public LinePaint(){

	}

	public void paint(Graphics2D g) {
		Stroke oldStroke = g.getStroke();
		g.setStroke(new BasicStroke(thickness));
		g.setColor(color);

		Point one = points[0];
		Point two = points[points.length - 1];
		g.drawLine((int)one.getX(), (int)one.getY(), (int)two.getX(), (int)two.getY());            


		g.setStroke(oldStroke);
	}

}
